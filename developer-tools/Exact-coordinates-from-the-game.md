How to get exact map coordinates from the game
--
1. "EnshroudedTable" is a pack of scripts and addresses to explore the game's memory called a cheat table. It's executed by [CheatEngine](https://www.cheatengine.org/downloads.php). Note: the CheatEngine might install bloatware, but I didn't find it.
1. Download [enshrouded.CT](EnshroudedTable 2.9.0 + Y-Z coordinate fix/enshrouded.CT) from here: I've fixed the Y,Z coordinates that were interchanged.
The original can be downloaded from [nexusmods](https://www.nexusmods.com/enshrouded/mods/10?tab=files).
1. Make a backup copy of your save files just in case. I haven't had savegame corruption, but better to be safe than sorry. It's also advisable to play with cheats in a new world or a copy, not in a world that had a lot of effort put into it.
1. When launching the program it will ask for administrator permissions. This is necessary to be able to read the game's memory.
Then it will ask for a CEShare URL. It's a dropdown (barely noticably), that has one option... Either leave it empty, or chose that if you'd like to explore more tables.
1. File -> Open Process. Choose enshrouded.
1. File -> Open File. Navigate to and select `enshrouded.CT`
1. Close the "Code list/Pause" window.
1. Find "coordinates and speed" in the list at the bottom of the program window. When in a world this shows:  `x`=longitude, `y`=latitude, `z`=height.

There are more cheats in the list, like:
* 'inifinite mana' - good for flying high
* 'infinite fog timer' - allows exploring deadly shroud
* 'infinite bound timer' - allows leaving the early access area
* 'remove fall damage' - makes exploring the hills safer
* 'high speed'
* 'fog area' - allows building in shroud
* 'flying' - no falling, so can't go down, only up until disabled

Infinite mana (probably others) used to cause crashes, eg. when fast traveling. Observing the coordinates caused no issues.
